#!/usr/bin/env bash
#
# New wrapper for refractainstaller-yad 9.5.0

TEXTDOMAIN=refractainstaller-gui
TEXTDOMAINDIR=/usr/share/locale/

if ! [[ -f /usr/bin/yad ]]; then
	xterm -fa mono -fs 14 -geometry 80x20+0+0 -e 'echo $"Yad is not installed. 
Install it or run the text-only refractainstaller in a root terminal."' &
	exit 0
fi

installer="/usr/bin/refractainstaller-yad"


# Start script in terminal with sudo. Most live systems have sudo nopasswd.
# If sudo fails, ask for root password.
xterm -fa mono -fs 12 -e  "echo 'If user password fails, use root password.' && sudo  $installer ||  su -c $installer"


exit 0

